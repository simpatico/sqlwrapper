/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mysimpatico.sqlwrapper;

import com.mysimpatico.sqlwrapper.SqlWrapper.SelectOption;

/**
 *
 * @author simpatico
 */
public class SqlWrapper1 {

    public static SelectCoreQuery select(final SelectOption selectOptions, final Column[] cols, final SelectableFrom[] tables) {
        final SelectCoreQueryBuilder selectCoreQueryBuilder = new SelectCoreQueryBuilder();
        selectCoreQueryBuilder.buildResult(tables, cols);
        selectCoreQueryBuilder.buildSelectOption(selectOptions);
        return (SelectCoreQuery) selectCoreQueryBuilder.getResult();
    }

    SelectQuery intersect(final SelectCoreQuery selectCoreQuery, final SelectCoreQuery selectCoreQuery1) {
        return selectCoreQuery.setOperator(SelectQuery.setOperators.INTERSECT, selectCoreQuery1);
    }

    SelectQuery except(final SelectCoreQuery selectCoreQuery, final SelectCoreQuery selectCoreQuery1) {
        return selectCoreQuery.setOperator(SelectQuery.setOperators.EXCEPT, selectCoreQuery1);
    }

    SelectQuery union(final SelectCoreQuery selectCoreQuery, final SelectCoreQuery selectCoreQuery1) {
        return selectCoreQuery.setOperator(SelectQuery.setOperators.UNION, selectCoreQuery1);
    }
}
