package com.mysimpatico.sqlwrapper;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author simpatico
 */
public class SelectQuery implements Component{
    public enum setOperators {
        INTERSECT, UNION, EXCEPT
    }

     SelectCoreQuery selectCoreQuery, selectCoreQuery1;
     SelectQuery.setOperators setOperator;
     SqlWrapper.Sort order;
     CommaList orderBy;

    @Override
    public String getName(){
        return selectCoreQuery.getName() + printSetOperator() + printOrder();
    }

    public String printOrder(){
        return order == null? "":" ORDER BY " + orderBy.getName() + " " + order;
    }

    public String printSetOperator(){
        return setOperator == null? "": setOperator + selectCoreQuery1.getName();
    }

    public void setOrder(final CommaList orderBy, final SqlWrapper.Sort order){
        this.orderBy = orderBy;
        this.order = order;
    }

    protected SelectQuery(final SelectCoreQuery selectCoreQuery, final SelectQuery.setOperators setOperator, final SelectCoreQuery selectCoreQuery1){
        this.selectCoreQuery = selectCoreQuery;
        this.selectCoreQuery1 = selectCoreQuery1;
        this.setOperator = setOperator;
    }

    protected SelectQuery(){

    }
}
