package com.mysimpatico.sqlwrapper;

import com.mysimpatico.sqlwrapper.SqlWrapper;

public class View implements SelectableFrom {

	private final String name;
	private final String selectQuery;
	private final Column[] columns;
	
	public View(final String name, final String selectQuery){
		this.name = name;
		this.selectQuery = selectQuery;
		columns = null;
	}
	/**
	 * Will create a view whose id column is that of the first table in the tables array.
	 * @param name
	 * @param whats
	 * @param tables
	 * @param whereColumns
	 * @param whereValues
	 */
	public View(final String name, final Column[] whats, final SelectableFrom[] tables, final Column[] whereColumns, final String[] whereValues){
		this.name = name;
		this.columns = whats;
		selectQuery = SqlWrapper.select(whats, tables, whereColumns, whereValues);
	}
	
	public View(final String name, final Column[] whats, final SelectableFrom[] tables, final SelectableFrom[] whereTables, final Column[] whereColumns, final SelectableFrom[] whereTables1, final Column[] whereColumns1){
		this.name = name;
		this.columns = whats;
		selectQuery = SqlWrapper.join(whats, tables, whereTables, whereColumns, whereTables1, whereColumns1);
	}
	
	@Override
	public String toString(){
		return name + " AS " + selectQuery;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Column[] getColumns() {
		return columns;
	}
	
	@Override
	public boolean supportBoolean() {
		for (Column c: columns) if (c.getType().equals(SqlWrapper.Type.BOOLEAN)) return true;
		return false;
	}

    @Override
    public Column getIdColumn() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
