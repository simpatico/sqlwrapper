/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mysimpatico.sqlwrapper;

/**
 *
 * @author simpatico
 */
public class Expression implements Component {

    protected String literalValue;

    public Expression(final String literalValue) {
        this.literalValue = literalValue;
    }

    @Override
    public String getName() {
        return "\"" + literalValue + "\"";
    }

    @Override
    public String toString(){
        return getName();
    }
}
