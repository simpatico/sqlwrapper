/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mysimpatico.sqlwrapper;

/**
 *
 * @author simpatico
 */
public class BinaryExpression extends Composite{

    public BinaryExpression(final Expression exp1, final String binaryOperator, final Expression exp2){
        this(binaryOperator);
        addNode(exp1);
        addNode(exp2);
    }

    public BinaryExpression(final String binaryOperator){
        super("", binaryOperator, "");
    }
}
