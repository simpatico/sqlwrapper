/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mysimpatico.sqlwrapper;

import com.mysimpatico.sqlwrapper.SqlWrapper.SelectOption;

/**
 *
 * @author simpatico
 */
public class SelectCoreQuery extends SelectQuery {

    SelectOption selectOption;
    CommaList fromList;
    CommaList resultList;

    public SelectCoreQuery(final CommaList fromList,final SelectOption selectOption, final CommaList resultList) {
        this.selectOption = selectOption;
        this.resultList = resultList;
        this.fromList = fromList;
    }

    private String getResultList() {
        if (selectOption == SelectOption.NONE) {
            return resultList.toString();
        }
        return selectOption + "(" + resultList + ")";
    }

    SelectQuery setOperator(SelectQuery.setOperators setOperator, final SelectCoreQuery selectCoreQuery) {
        return new SelectQuery(this, setOperator,  selectCoreQuery);
    }

    SelectQuery intersect(final SelectCoreQuery selectCoreQuery){
        return setOperator(SelectQuery.setOperators.INTERSECT, selectCoreQuery);
    }

    SelectQuery except(final SelectCoreQuery selectCoreQuery){
        return setOperator(SelectQuery.setOperators.EXCEPT, selectCoreQuery);
    }

    SelectQuery union(final SelectCoreQuery selectCoreQuery){
        return setOperator(SelectQuery.setOperators.UNION, selectCoreQuery);
    }

    @Override
    public String getName() {
        return "SELECT " + getResultList() + " FROM " + fromList;
    }
}
