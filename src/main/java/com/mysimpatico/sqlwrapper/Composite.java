/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mysimpatico.sqlwrapper;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author simpatico
 */
public abstract class Composite implements Component {
    public List<Component> children = new LinkedList<Component>();
    private final String separator;
    private final String prefix;
    private final String postfix;

    public Composite(final String prefix, final String separator, final String postfix){
        this.prefix = prefix;
        this.separator = separator;
        this.postfix = postfix;
    }

    public void addNode(final Component node) {
        if (!children.contains(node)) {
            children.add(node);
        }
    }

    public void addNodes(final Component[] nodes) {
        for (Component node : nodes) {
            addNode(node);
        }
    }

    @Override
    public String getName() {
        return toString();
    }

    @Override
    public String toString() {
        String ret = prefix;
        boolean first = true;

        for (Component child : children) {
            if (!first) {
                ret += " " + separator + " ";
            } else {
                first = false;
            }
            ret += child.getName();
        }
        return ret + postfix;
    }

}
