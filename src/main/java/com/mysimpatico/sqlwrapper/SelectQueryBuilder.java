/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mysimpatico.sqlwrapper;

import com.mysimpatico.sqlwrapper.SqlWrapper.SelectOption;

/**
 *
 * @author simpatico
 */
public class SelectQueryBuilder extends SelectCoreQueryBuilder{

    private final SelectQuery selectQuery = new SelectQuery();
    private final SelectCoreQueryBuilder coreQueryBuilder = new SelectCoreQueryBuilder();

    @Override
    public void buildResult(final SelectableFrom[] tables, final Column[] cols){
        coreQueryBuilder.buildResult(tables, cols);
    }

    @Override
    public void buildSelectOption(final SelectOption selectOption){
        coreQueryBuilder.buildSelectOption(selectOption);
    }

    @Override
    protected void buildFromList(final SelectableFrom[] tables){
        coreQueryBuilder.buildFromList(tables);
    }

    protected void buildSortOrder(final SqlWrapper.Sort order){
        
    }

    @Override
    public SelectQuery getResult(){
        selectQuery.selectCoreQuery = (SelectCoreQuery) coreQueryBuilder.getResult();
        return selectQuery;
    }
}
