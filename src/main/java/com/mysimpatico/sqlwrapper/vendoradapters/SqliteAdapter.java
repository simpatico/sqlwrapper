/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mysimpatico.sqlwrapper.vendoradapters;

import com.mysimpatico.sqlwrapper.Column;
import com.mysimpatico.sqlwrapper.SqlWrapper;
import com.mysimpatico.sqlwrapper.SqlWrapper.ConflictResolution;
import java.util.Date;

/**
 *
 * @author simpatico
 */
public class SqliteAdapter extends VendorAdapter{

    @Override
    public String getDate() {
        return new Date(System.currentTimeMillis() + millisTestOffset).toString();//"CURRENT_DATE ";
    }

    @Override
    public String escapeString(String string) {
        return "\"" + string + "\"";
    }

    @Override
    public String getOnConflict(ConflictResolution res, boolean update) {
        return "";
    }

    @Override
    protected String getDriverName() {
        return "org.sqlite.JDBC";
    }

    @Override
    protected String getConnectionURLPrefix() {
        return "jdbc:sqlite:";
    }

    @Override
    public String getPrimaryKeyDeclaration() {
        return "INTEGER PRIMARY KEY AUTOINCREMENT";
    }

    @Override
    public String getPrimaryKey(final Column[] primaryKeys){
        return "UNIQUE (" + SqlWrapper.joinColumns(primaryKeys, SqlWrapper.QueryType.FOREIGN) + ") CHECK(" + SqlWrapper.joinColumns(primaryKeys, SqlWrapper.QueryType.PRIMARY) + ")";
    }

}
