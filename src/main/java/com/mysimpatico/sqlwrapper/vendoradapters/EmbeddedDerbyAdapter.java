/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mysimpatico.sqlwrapper.vendoradapters;

/**
 *
 * @author simpatico
 */
public class EmbeddedDerbyAdapter extends VendorAdapter{

    @Override
    protected String getDriverName() {
        return "org.apache.derby.jdbc.EmbeddedDriver";
    }

    @Override
    protected String getConnectionURLPrefix() {
        return "jdbc:derby:";
    }

    @Override
    public String getPrimaryKeyDeclaration() {
        return "INT NOT NULL GENERATED ALWAYS AS IDENTITY";
    }

}
