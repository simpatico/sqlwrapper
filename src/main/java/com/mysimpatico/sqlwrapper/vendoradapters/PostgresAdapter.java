/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mysimpatico.sqlwrapper.vendoradapters;

import com.mysimpatico.sqlwrapper.Column;
import com.mysimpatico.sqlwrapper.SqlWrapper;

/**
 *
 * @author simpatico
 */
public class PostgresAdapter extends VendorAdapter{

    @Override
    public String getPrimaryKeyDeclaration() {
        //return columns[i].getType() + " UNIQUE";
        return "SERIAL UNIQUE";
    }

    @Override
    protected String getDriverName() {
        return "org.postgresql.Driver";
    }

    @Override
    protected String getConnectionURLPrefix() {
       return "jdbc:jdc:";
    }

    @Override
    public Column getRidColumn(){
        return new Column(SqlWrapper.rid, SqlWrapper.Type.SERIAL);
    }


}
