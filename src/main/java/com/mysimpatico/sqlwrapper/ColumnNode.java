/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mysimpatico.sqlwrapper;

/**
 *
 * @author simpatico
 */
public class ColumnNode extends Expression{

    public ColumnNode(final Column col){
        this(null,col);
    }

    public ColumnNode(final SelectableFrom table, final Column col){
        super(((table != null)? table.getName() + "."  :"") + col.getName());
    }

    @Override
    public String getName(){
        return literalValue;
    }
}
