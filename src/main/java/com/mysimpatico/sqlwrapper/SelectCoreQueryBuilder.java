/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mysimpatico.sqlwrapper;

import com.mysimpatico.sqlwrapper.SqlWrapper.SelectOption;

/**
 *
 * @author simpatico
 */
public class SelectCoreQueryBuilder{

    private SelectCoreQuery result;

    public void buildResult(final SelectableFrom[] tables, final Column[] cols){
       final CommaList resultList = new CommaList();
        for(int i=0; i<cols.length; i++){
            resultList.addNode(new ColumnNode(tables[i], cols[i]));
        }
       result = new SelectCoreQuery(null,SqlWrapper.SelectOption.NONE, resultList);
       buildFromList(tables);
    }

    protected void buildFromList(final SelectableFrom[] tables){
        final CommaList tablesList = new CommaList();
        tablesList.addNodes(tables);
        result.fromList = tablesList;
    }

    public void buildSelectOption(final SelectOption selectOption){
        result.selectOption = selectOption;
    }

    public SelectQuery getResult(){
        return result;
    }

}
