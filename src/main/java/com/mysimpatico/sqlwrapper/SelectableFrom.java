package com.mysimpatico.sqlwrapper;

public interface SelectableFrom extends Component{
	String getName();
	Column[] getColumns();
        Column getIdColumn();
	boolean supportBoolean();
}
