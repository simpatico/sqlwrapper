/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mysimpatico.sqlwrapper;

/**
 *
 * @author simpatico
 */
public interface Component {
    @Override
    public String toString();
    public String getName();
}
