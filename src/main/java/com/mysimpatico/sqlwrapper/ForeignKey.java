/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mysimpatico.sqlwrapper;

/**
 *
 * @author simpatico
 */
class ForeignKey extends Column {

    private final Table refTable;
    private final Column refField;

    ForeignKey(final boolean unique, final String name, final int length, final SqlWrapper.Type type, final Object def, final Table refTable, final Column refField, final boolean notNull, final String condition) {
        super(unique,name,length,type,def,notNull,condition);
        this.refTable = refTable;
        this.refField = refField;
    }


    public Table getRefTable() {
        return refTable;
    }

    private String getReference() {
        if (refField != null) {
            return " REFERENCES " + refTable.getName() + "(" + refField.getName() + ") " + Table.getOnDelete();
        } else {
            return "";
        }
    }

    @Override
    public String toString() {
        return name + " " + type + getLength() + getDefString() + getUnique() + getNotNull() + getReference() + getCondition();
    }


}
