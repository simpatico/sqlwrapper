package com.mysimpatico.sqlwrapper;

import com.mysimpatico.sqlwrapper.vendoradapters.SqliteAdapter;
import static org.junit.Assert.*;

import java.io.File;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SqlWrapperTest {

    private File dbFile = new File("db.sqlwrapper");
    private File dbFile2 = new File("db.sqlite-journal");
    private File dbFile3 = new File("derby.log");

    @Before
    public void setUp() {
        dbFile.deleteOnExit();
        dbFile2.deleteOnExit();
        dbFile3.deleteOnExit();
        SqlWrapper.setVendor(new SqliteAdapter());
    }

    @After
    public void tearDown() {
        dbFile.delete();
        dbFile2.delete();
        dbFile3.delete();
    }

    @Test
    public void testDate() {
        try {
            ResultSet rs;
            final Connection con = SqlWrapper.connect();

            final Statement st = con.createStatement();
            final String startDate = "start_date";
            final Column sessionStartTimeColumn = new Column(true, startDate, SqlWrapper.DATE);
            final String TEST_TABLE = "TEST_TABLE";
            final Table table = new Table(TEST_TABLE, sessionStartTimeColumn);

            st.execute(SqlWrapper.create(table));

            final String[] values = {"(DATETIME('now', 'localtime'))"};
           // st.executeUpdate(SqlWrapper.insert(table, values[0])); will not work, should investigate into this dirty string passing
            st.executeUpdate("insert into " + table.getName() + "("  + sessionStartTimeColumn.getName()+ ") VALUES((DATETIME('now', 'localtime')))");
            rs = st.executeQuery(SqlWrapper.select(sessionStartTimeColumn, table));
            String ds = rs.getString(sessionStartTimeColumn.getName());
            final Timestamp d = Timestamp.valueOf(ds);

            System.out.println(ds);
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.set(Calendar.MILLISECOND, 0);
            cal.set(Calendar.SECOND, 0);
            long rr = cal.getTimeInMillis();

            Calendar cal1 = Calendar.getInstance();
            cal1.set(Calendar.MILLISECOND, 0);
            cal1.set(Calendar.SECOND, 0);
            long cc = cal1.getTimeInMillis();
                assertEquals(cc, rr);

            st.execute(SqlWrapper.dropTable(table));
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }
/* should look into time insertion in SQLite.
    @Test
    public void firstLastTest() {
        final String postId = "p_id";
        final String startDate = "start_date";
        final String endDate = "end_date";

        final Column postIdColumn = new Column(true, postId, SqlWrapper.INT);
        final Column sessionStartTimeColumn = new Column(true, startDate, SqlWrapper.DATE);
        final Column sessionEndTimeColumn = new Column(true, endDate, SqlWrapper.DATE);
        final Column[] columns = {sessionStartTimeColumn, sessionEndTimeColumn, postIdColumn};
        final Column[] timeColumns = {sessionStartTimeColumn, sessionEndTimeColumn};
        final String TEST_TABLE = "TEST_TABLE";
        final Table table = new Table(TEST_TABLE, columns);
        final int n = 5;

        try {
            ResultSet rs;
            final Connection con = SqlWrapper.connectToSQLite();

            final Statement st = con.createStatement();
            st.execute(SqlWrapper.create(table));

            TimeZone tz = TimeZone.getTimeZone("GMT");

            DateFormat dfGMT = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
            dfGMT.setTimeZone(tz);

            GregorianCalendar gc = new GregorianCalendar();
            int offset = gc.get(Calendar.ZONE_OFFSET);
            gc.set(Calendar.SECOND, 0);
            gc.set(Calendar.MILLISECOND, 0);
            long cc = gc.getTimeInMillis() - offset;

            Date first = null;
            long ppid = 0;
            for (int i = 0; i < n; i++) {
                cc = gc.getTimeInMillis();
                // cc -= offset;
                long pid = System.currentTimeMillis(); //used as post uid
                final String[] values = {"(DATETIME('now', 'localtime'))", "(DATETIME('now', 'localtime'))", Long.toString(pid)};
                st.executeUpdate(SqlWrapper.insert(table, columns, values));
                rs = st.executeQuery(SqlWrapper.select(timeColumns, table));
                java.sql.Date d = rs.getDate(sessionStartTimeColumn.getName());
                System.out.println(d);
                if (i == 0) {
                    // first = d;
                    ppid = pid;
                }
                Calendar cal = Calendar.getInstance();
                cal.setTime(d);
                cal.set(Calendar.MILLISECOND, 0);
                cal.set(Calendar.SECOND, 0);
                long rr = d.getTime();
                assertEquals(cc, rr);
                rs = st.executeQuery(SqlWrapper.last(sessionStartTimeColumn, table));
                Timestamp result1 = Timestamp.valueOf(rs.getObject(SqlWrapper.index).toString());
                result1.setNanos(0);
                cal.setTime(d);
                cal.set(Calendar.SECOND, 0);
                long ss = cal.getTimeInMillis();
                assertEquals(ss, cc);
                rs = st.executeQuery(SqlWrapper.last(sessionStartTimeColumn, table, postIdColumn, pid));
                Timestamp result2 = Timestamp.valueOf(rs.getObject(sessionStartTimeColumn.getName()).toString());
                assertEquals(result2, result1);
                Thread.sleep(1000);
            }
            rs = st.executeQuery(SqlWrapper.first(sessionStartTimeColumn, table, postIdColumn, ppid));
            Timestamp result = Timestamp.valueOf(rs.getObject(sessionStartTimeColumn.getName()).toString());
            assertEquals(result.getTime(), first.getTime());

            st.execute(SqlWrapper.dropTable(table));
            con.commit();
            con.close();

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }*/

    @Test
    public void testCreateSimpleTable() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

        final String TEST_TABLE = "TEST_TABLE";
        final String TEST_FIELD = "TEST_FIELD";
        final Column column = new Column(TEST_FIELD, SqlWrapper.TEXT);
        final Column column1 = new Column(TEST_FIELD, SqlWrapper.INT);
        final Column column2 = new Column(TEST_FIELD, SqlWrapper.BOOL);
        final Column column3 = new Column(TEST_FIELD, SqlWrapper.DATE);
        final Table table = new Table(TEST_TABLE, column);
        final Table table1 = new Table(TEST_TABLE, column1);
        final Table table2 = new Table(TEST_TABLE, column2);
        final Table table3 = new Table(TEST_TABLE, column3);

        final Connection con = SqlWrapper.connect();
        con.setAutoCommit(false);
        final Statement st = con.createStatement();

        st.execute(SqlWrapper.create(table));
        st.executeQuery(SqlWrapper.selectAll(table));
        st.execute(SqlWrapper.dropTable(table));

        st.execute(SqlWrapper.create(table1));
        st.executeQuery(SqlWrapper.selectAll(table1));
        st.execute(SqlWrapper.dropTable(table1));

        st.execute(SqlWrapper.create(table2));
        st.executeQuery(SqlWrapper.selectAll(table2));
        st.execute(SqlWrapper.dropTable(table2));

        st.execute(SqlWrapper.create(table3));
        st.execute(SqlWrapper.selectAll(table3));
        st.execute(SqlWrapper.dropTable(table3));

        con.commit();
        con.close();
    }

    @Test
    public void testCreateTable2FieldsIntDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

        final String TEST_TABLE = "TEST_TABLE";
        final String TEST_FIELD = "TEST_FIELD";
        final String TEST_FIELD1 = "TEST_FIELD1";
        final Column column = new Column(TEST_FIELD, SqlWrapper.TEXT, "Abschluss");
        final Column column1 = new Column(TEST_FIELD1, SqlWrapper.INT, 0);
        final Column[] columns = {column, column1};
        final Table table = new Table(TEST_TABLE, columns);

        final Connection con = SqlWrapper.connect();
        con.setAutoCommit(false);
        final Statement st = con.createStatement();
        st.execute(SqlWrapper.create(table));
        st.executeQuery(SqlWrapper.selectAll(table));
        st.execute(SqlWrapper.dropTable(table));

        final String TEST_FIELD2 = "TEST_FIELD2";
        final Column column2 = new Column(TEST_FIELD2, SqlWrapper.BOOL, true, true);
        final Column[] columns1 = {column, column1, column2};
        final String TEST_TABLE1 = "TEST_TABLE1";
        final Table table1 = new Table(TEST_TABLE1, columns1);

        st.execute(SqlWrapper.create(table1));
        st.executeQuery(SqlWrapper.selectAll(table1));
        st.execute(SqlWrapper.dropTable(table1));

        final Table table2 = new Table(TEST_TABLE, column1);

        st.execute(SqlWrapper.create(table2));
        st.executeUpdate(SqlWrapper.insert(table2));
        st.execute(SqlWrapper.dropTable(table2));

        con.commit();
        con.close();
    }

    @Test
    public void testCreateTableReferenceField() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {

        final String TEST_TABLE = "TEST_TABLE";
        final String TEST_FIELD = "TEST_FIELD";
        final String TEST_REFTABLE = "TEST_TABLE1";
        final String TEST_REFFIELD = "TEST_FIELD1";

        final Column column = new Column(TEST_REFFIELD, SqlWrapper.INT);
        final Table table = new Table(TEST_REFTABLE, column);

        final Connection con = SqlWrapper.connect();
        con.setAutoCommit(false);
        final Statement st = con.createStatement();

        st.execute(SqlWrapper.create(table));
        st.executeQuery(SqlWrapper.selectAll(table));

        final Column column1 = Column.createForeignKey(TEST_FIELD, table, column);
        final Table table1 = new Table(TEST_TABLE, column1);
        st.execute(SqlWrapper.create(table1));
        st.executeQuery(SqlWrapper.selectAll(table1));

        st.execute(SqlWrapper.dropTable(table));
        st.execute(SqlWrapper.dropTable(table1));

        con.commit();
        con.close();
    }

    @Test
    public void createTableReferencedPrimaryKeysTest() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

        final String TEST_TABLE = "TEST_TABLE";
        final String TEST_FIELD = "TEST_FIELD";
        final String TEST_REFTABLE = "TEST_TABLE1";
        final String TEST_REFTABLE1 = "TEST_TABLE2";

        final String TEST_REFFIELD = "TEST_FIELD1";
        final String TEST_REFFIELD1 = "TEST_FIELD2";

        final String TEST_FIELD1 = "TEST_FIELD3";

        final Column column = new Column(TEST_REFFIELD, SqlWrapper.INT);
        final Table table = new Table(TEST_REFTABLE, column);

        final Connection con = SqlWrapper.connect();
        con.setAutoCommit(false);
        final Statement st = con.createStatement();

        st.execute(SqlWrapper.create(table));
        st.executeQuery(SqlWrapper.selectAll(table));

        final Column column1 = new Column(TEST_REFFIELD1, SqlWrapper.INT);
        final Table table1 = new Table(TEST_REFTABLE1, column1);

        st.execute(SqlWrapper.create(table1));
        st.executeQuery(SqlWrapper.selectAll(table1));

        final Column column2 = Column.createForeignKey(TEST_FIELD, table, column);
        final Column column3 = Column.createForeignKey(TEST_FIELD1, table1, column1);
        final Column[] columns = {column2, column3};
        final Table table2 = new Table(TEST_TABLE, columns, columns);

        st.execute(SqlWrapper.create(table2));
        st.executeQuery(SqlWrapper.selectAll(table2));

        st.execute(SqlWrapper.dropTable(table2));
        st.execute(SqlWrapper.dropTable(table1));
        st.execute(SqlWrapper.dropTable(table));

        st.execute(SqlWrapper.create(table));
        st.executeQuery(SqlWrapper.selectAll(table));

        st.execute(SqlWrapper.create(table1));
        st.executeQuery(SqlWrapper.selectAll(table1));

        final Table table3 = new Table(TEST_TABLE, columns);
        st.execute(SqlWrapper.create(table3));
        st.executeQuery(SqlWrapper.selectAll(table3));

        st.execute(SqlWrapper.dropTable(table3));
        st.execute(SqlWrapper.dropTable(table1));
        st.execute(SqlWrapper.dropTable(table));

        con.commit();
        con.setAutoCommit(true);
        st.close();
        con.close();
    }

    @Test
    public void testCreateTablesWithPrimaryKeyForeignKeyInsertDate() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {

        final String WORDS_TABLE = "words";
        final String DEF_TABLE = "definitions";
        final String MENG_TABLE = "meanings";
        final String CYC_TABLE = "cycle";
        final String SEQ_TABLE = "sequence";
        final String SCHED_TABLE = "scheduler";

        final String LAST_SCHED = "time";

        final String WORDS_ID = "w_id";
        final String WORD = "word";

        final String DEF_ID = "d_id";
        final String DEF = "def";

        final String CYC_NO = "c_id";

        final String PUBLISHED = "published";

        final String PUB_COUNT = "publishedCount";

        final Column wordColumn = new Column(WORD, SqlWrapper.TEXT);
        final Table wordsTable = new Table(WORDS_TABLE, wordColumn);

        final Column defColumn = new Column(DEF, SqlWrapper.TEXT);
        final Table defTable = new Table(DEF_TABLE, defColumn);

        final Column wordsIdColumn = Column.createForeignKey(WORDS_ID, wordsTable, SqlWrapper.ROWID);
        final Column defIdColumn = Column.createForeignKey(DEF_ID, defTable, SqlWrapper.ROWID);

        final Column[] mengColumns = {wordsIdColumn, defIdColumn};
        final Table mengTable = new Table(MENG_TABLE, mengColumns, mengColumns);

        final Column publishedColumn = new Column(PUBLISHED, SqlWrapper.BOOL);
        final Column publishCount = new Column(PUB_COUNT, SqlWrapper.INT, 0);
        final Column[] seqColumns = {publishedColumn, publishCount};
        final Table seqTable = new Table(SEQ_TABLE, seqColumns);

        final Column cycNoColumn = Column.createForeignKey(CYC_NO, seqTable, SqlWrapper.ROWID);
        final Column[] cycleColumns = {cycNoColumn, wordsIdColumn, defIdColumn};
        final Table cycleTable = new Table(CYC_TABLE, cycleColumns, mengTable, mengColumns);

        final Column lastSchedColumn = new Column(LAST_SCHED, SqlWrapper.DATE);
        final Table schedTable = new Table(SCHED_TABLE, lastSchedColumn);

        final Connection con = SqlWrapper.connect();
        con.setAutoCommit(false);
        final Statement st = con.createStatement();

        st.execute(SqlWrapper.create(wordsTable));
        st.execute(SqlWrapper.create(defTable));
        st.execute(SqlWrapper.create(mengTable));
        st.execute(SqlWrapper.create(seqTable));
        st.execute(SqlWrapper.create(cycleTable));
        st.execute(SqlWrapper.create(schedTable));
        st.execute((SqlWrapper.insert(schedTable, lastSchedColumn, new Date(System.currentTimeMillis()))));

        st.execute(SqlWrapper.dropTable(schedTable));
        st.execute(SqlWrapper.dropTable(mengTable));
        st.execute(SqlWrapper.dropTable(seqTable));
        st.execute(SqlWrapper.dropTable(cycleTable));
        st.execute(SqlWrapper.dropTable(wordsTable));
        st.execute(SqlWrapper.dropTable(defTable));

        con.commit();
        con.close();
    }

    @Test
    public void testCreateTableBothFieldsReference() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {

        final String TEST_TABLE = "TEST_TABLE";
        final String TEST_FIELD = "TEST_FIELD";
        final String TEST_FIELD1 = "TEST_FIELD1";

        final String TEST_REFTABLE = "TEST_TABLE1";
        final String TEST_REFTABLE1 = "TEST_TABLE2";

        final String TEST_REFFIELD = "TEST_FIELD1";
        final String TEST_REFFIELD1 = "TEST_FIELD2";

        final Column column = new Column(TEST_REFFIELD, SqlWrapper.INT);
        final Table table = new Table(TEST_REFTABLE, column);

        final Connection con = SqlWrapper.connect();
        con.setAutoCommit(false);
        final Statement st = con.createStatement();

        st.execute(SqlWrapper.create(table));
        st.executeQuery(SqlWrapper.selectAll(table));

        final Column column1 = new Column(TEST_REFFIELD1, SqlWrapper.INT);
        final Table table1 = new Table(TEST_REFTABLE1, column1);

        st.execute(SqlWrapper.create(table1));
        st.executeQuery(SqlWrapper.selectAll(table1));

        final Column column2 = Column.createForeignKey(TEST_FIELD, table, column);
        final Column column3 = Column.createForeignKey(TEST_FIELD1, table1, column1);
        final Column[] columns = {column2, column3};

        final Table table2 = new Table(TEST_TABLE, columns);
        st.execute(SqlWrapper.create(table2));
        st.executeQuery(SqlWrapper.selectAll(table2));

        st.execute(SqlWrapper.dropTable(table2));
        st.execute(SqlWrapper.dropTable(table));
        st.execute(SqlWrapper.dropTable(table1));

        con.commit();
        con.close();
    }

    @Test
    public void testCreateTable3ReferencesForeignKey2() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

        final String TEST_TABLE = "TEST_TABLE";
        final String TEST_REFTABLE = "TEST_REFTABLE";
        final String TEST_REFTABLE1 = "TEST_REFTABLE1";
        final String TEST_REFTABLE2 = "TEST_REFTABLE2";

        final String TEST_FIELD = "TEST_FIELD";
        final String TEST_FIELD1 = "TEST_FIELD1";
        final String TEST_FIELD2 = "TEST_FIELD2";

        final String TEST_REFFIELD = "TEST_REFFIELD";
        final String TEST_REFFIELD1 = "TEST_REFFIELD1";
        final String TEST_REFFIELD2 = "TEST_REFFIELD2";

        final String TEST_FORTABLE = "TEST_FORTABLE";

        final Column column = new Column(TEST_REFFIELD, SqlWrapper.INT);
        final Table table = new Table(TEST_REFTABLE, column);

        final Connection con = SqlWrapper.connect();
        con.setAutoCommit(false);
        final Statement st = con.createStatement();

        st.execute(SqlWrapper.create(table));
        st.executeQuery(SqlWrapper.selectAll(table));

        final Column column1 = new Column(TEST_REFFIELD1, SqlWrapper.INT);
        final Table table1 = new Table(TEST_REFTABLE1, column1);

        st.execute(SqlWrapper.create(table1));
        st.executeQuery(SqlWrapper.selectAll(table1));

        final Column column2 = new Column(TEST_REFFIELD2, SqlWrapper.INT);
        final Table table2 = new Table(TEST_REFTABLE2, column2);

        st.execute(SqlWrapper.create(table2));
        st.executeQuery(SqlWrapper.selectAll(table2));

        final Column[] columns = {column1, column2};
        final Table table3 = new Table(TEST_FORTABLE, columns, columns);

        st.execute(SqlWrapper.create(table3));
        st.executeQuery(SqlWrapper.selectAll(table3));

        final Column column3 = Column.createForeignKey(TEST_FIELD, table, column);
        final Column column4 = Column.createForeignKey(TEST_FIELD1, table1, column1);
        final Column column5 = Column.createForeignKey(TEST_FIELD2, table2, column2);
        final Column[] columns1 = {column3, column4, column5};
        final Column[] columns2 = {column4, column5};

        final Table table4 = new Table(TEST_TABLE, columns1, table3, columns2);

        st.execute(SqlWrapper.create(table4));
        st.executeQuery(SqlWrapper.selectAll(table4));

        st.execute(SqlWrapper.dropTable(table3));
        st.execute(SqlWrapper.dropTable(table1));
        st.execute(SqlWrapper.dropTable(table2));
        st.execute(SqlWrapper.dropTable(table));
        st.execute(SqlWrapper.dropTable(table4));

        con.commit();
        con.close();
    }

    @Test
    public void testInsertInts() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {


        final String TEST_FIELD = "TEST_FIELD";
        final String TEST_FIELD1 = "TEST_FIELD1";
        final String TEST_FIELD2 = "TEST_FIELD2";

        final String TEST_TABLE = "TEST_TABLE";

        final Column column = new Column(TEST_FIELD, SqlWrapper.INT);
        final Column column1 = new Column(TEST_FIELD1, SqlWrapper.INT);
        final Column[] columns = {column, column1};
        final Table table = new Table(TEST_TABLE, columns);

        final Connection con = SqlWrapper.connect();
        con.setAutoCommit(false);
        final Statement st = con.createStatement();

        st.execute(SqlWrapper.create(table));
        st.execute(SqlWrapper.select(column, table));
        int[] values = {6, 7};
        st.executeUpdate(SqlWrapper.insert(table, columns, values));
        st.executeQuery(SqlWrapper.select(column, table));
        st.execute(SqlWrapper.dropTable(table));

        final Table table1 = new Table(TEST_TABLE, column);
        st.execute(SqlWrapper.create(table1));
        st.executeUpdate(SqlWrapper.insert(table, column, 7));
        st.executeUpdate(SqlWrapper.update(table1, column, 5, column, 7));
        st.executeQuery(SqlWrapper.select(column, table1));
        st.execute(SqlWrapper.dropTable(table1));

        final Column column2 = new Column(TEST_FIELD2, SqlWrapper.INT);
        final Column[] columns1 = {column, column1, column2};
        final Table table2 = new Table(TEST_TABLE, columns1);

        final int[] values1 = {7, 8, 9};
        st.execute(SqlWrapper.create(table2));
        st.executeUpdate(SqlWrapper.insert(table2, columns1, values1));
        st.executeQuery(SqlWrapper.select(column, table2));
        st.execute(SqlWrapper.dropTable(table2));
        final String TEST_FIELD3 = "TEST_FIELD3";
        final String TEST_FIELD4 = "TEST_FIELD4";
        final Column column3 = new Column(TEST_FIELD3, SqlWrapper.TEXT);
        final Column column4 = new Column(TEST_FIELD4, SqlWrapper.TEXT);
        final Column[] columns2 = {column3, column, column4};
        final Table table3 = new Table(TEST_TABLE, columns2);
        st.execute(SqlWrapper.create(table3));
        st.execute(SqlWrapper.selectAll(table3));
        final String[] values2 = {"Abstimmung", Integer.toString(6), "Bleistift"};
        st.executeUpdate(SqlWrapper.insert(table3, columns2, values2));
        st.executeQuery(SqlWrapper.select(column, table3));
        st.execute(SqlWrapper.dropTable(table3));

        con.commit();
        con.close();
    }

    @Test
    public void testInsertStringsDates() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {


        final String TEST_FIELD = "TEST_FIELD";
        final String TEST_FIELD1 = "TEST_FIELD1";
        final String TEST_FIELD2 = "TEST_FIELD2";

        final String TEST_TABLE = "TEST_TABLE";
        final String ROWID = "ROWID";

        final Column column = new Column(TEST_FIELD, SqlWrapper.TEXT);
        final Column column1 = new Column(TEST_FIELD1, SqlWrapper.TEXT);
        final Column[] columns = {column, column1};
        final Table table = new Table(TEST_TABLE, columns);

        final Connection con = SqlWrapper.connect();
        con.setAutoCommit(false);
        final Statement st = con.createStatement();

        st.execute(SqlWrapper.create(table));
        st.execute(SqlWrapper.select(column, table));

        final String[] values = {"Abitur", "Abschaffung"};
        st.executeUpdate(SqlWrapper.insert(table, columns, values));
        st.executeQuery(SqlWrapper.select(column, table));
        st.execute(SqlWrapper.dropTable(table));

        final Table table1 = new Table(TEST_TABLE, column);
        st.execute(SqlWrapper.create(table1));
        st.executeUpdate(SqlWrapper.insert(table, column, "unterschieden"));
        st.executeQuery(SqlWrapper.select(column, table1));
        st.execute(SqlWrapper.dropTable(table1));

        final String[] values1 = {"Tasche", "Aufenthalt", "Boden"};

        final Column column2 = new Column(TEST_FIELD2, SqlWrapper.TEXT);
        final Column[] columns1 = {column, column1, column2};
        final Table table2 = new Table(TEST_TABLE, columns1);
        st.execute(SqlWrapper.create(table2));
        st.executeUpdate(SqlWrapper.insert(table2, columns1, values1));
        st.executeQuery(SqlWrapper.select(column, table2));
        st.execute(SqlWrapper.dropTable(table2));

        final Column column3 = new Column(TEST_FIELD, SqlWrapper.DATE);
        final Table table3 = new Table(TEST_TABLE, column3);
        st.execute(SqlWrapper.create(table3));
        st.executeUpdate(SqlWrapper.insert(table3, column3, new Date(0)));
        final Column column4 = new Column(ROWID, SqlWrapper.INT);
        st.executeUpdate(SqlWrapper.update(table3, column3, new Date(System.currentTimeMillis()), column4, 0));
        st.executeQuery(SqlWrapper.select(column3, table3));
        st.execute(SqlWrapper.dropTable(table3));

        con.commit();
        con.close();
    }

    @Test
    public void testSelectWhere() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {


        final String TEST_FIELD = "TEST_FIELD";
        final String TEST_FIELD1 = "TEST_FIELD3";

        final String TEST_TABLE = "TEST_TABLE";

        final Column column = new Column(TEST_FIELD, SqlWrapper.INT);
        final Column column1 = new Column(TEST_FIELD1, SqlWrapper.TEXT);
        final Column[] columns = {column, column1};

        final Table table = new Table(TEST_TABLE, columns);

        final Connection con = SqlWrapper.connect();
        con.setAutoCommit(false);
        final Statement st = con.createStatement();

        st.execute(SqlWrapper.create(table));
        st.executeQuery(SqlWrapper.select(column, table, column1, "TEST"));
        st.executeQuery(SqlWrapper.select(column, table, column1, "TEST", SqlWrapper.ASC));
        st.executeQuery(SqlWrapper.select(column, table, column1, "TEST", SqlWrapper.DESC));
        st.execute(SqlWrapper.dropTable(table));

        con.commit();
        con.close();
    }

    @Test
    public void testJoin() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {


        final String TEST_FIELD = "TEST_FIELD";
        final String TEST_FIELD1 = "TEST_FIELD1";
        final String TEST_FIELD2 = "TEST_FIELD2";

        final String TEST_TABLE = "TEST_TABLE";
        final String TEST_TABLE1 = "TEST_TABLE1";
        final String TEST_TABLE2 = "TEST_TABLE2";

        final Column column = new Column(TEST_FIELD, SqlWrapper.INT);
        final Column column1 = new Column(TEST_FIELD1, SqlWrapper.INT);
        final Column column2 = new Column(TEST_FIELD2, SqlWrapper.INT);
        final Column[] columns = {column, column1};
        final Column[] columns1 = {column, column1, column2};

        final Table table = new Table(TEST_TABLE, columns);
        final Table table1 = new Table(TEST_TABLE1, columns);
        final Table table2 = new Table(TEST_TABLE2, columns1);
        final Table[] tables = {table, table1};
        final String[] values = {Integer.toString(0), Integer.toString(4)};

        final Connection con = SqlWrapper.connect();
        con.setAutoCommit(false);
        final Statement st = con.createStatement();
        st.execute(SqlWrapper.create(table));
        st.execute(SqlWrapper.create(table1));
        st.execute(SqlWrapper.create(table2));

        st.executeQuery(SqlWrapper.select(columns, tables, columns, values));

        st.execute(SqlWrapper.dropTable(table));
        st.execute(SqlWrapper.dropTable(table1));
        st.execute(SqlWrapper.dropTable(table2));

        con.commit();
        con.close();
    }

    @Test
    public void testCount() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

        final String TEST_TABLE = "TEST_TABLE";
        final String TEST_FIELD = "TEST_FIELD";
        final String TEST_FIELD1 = "TEST_FIELD1";
        final Column column = new Column(TEST_FIELD, SqlWrapper.TEXT);
        final Column column1 = new Column(TEST_FIELD1, SqlWrapper.INT);
        final Column columns[] = {column, column1};
        final Table table = new Table(TEST_TABLE, columns);

        final Connection con = SqlWrapper.connect();
        con.setAutoCommit(false);

        final Statement st = con.createStatement();
        st.execute(SqlWrapper.create(table));
        st.executeQuery(SqlWrapper.count(column, table, column1, 0));

        st.execute(SqlWrapper.dropTable(table));

        con.commit();
        con.close();
    }

    @Test
    public void stcal2Test() {
        final String groupId = "g_id";
        final String group = "groups";
        final String activityName = "name";
        final String maintenance = "maintenance";
        final Column activityNameColumn = new Column(activityName, SqlWrapper.TEXT);
        final Table groupTable = new Table(group, activityNameColumn);

        final String activities = "activities";
        final String total = "total";
        final String startDate = "start_date";
        final String endDate = "end_date";
        final String ALL = "all";

        final Column groupIdColumn = Column.createForeignKey(groupId, groupTable, SqlWrapper.ROWID);
        final Column totalColumn = new Column(total, SqlWrapper.INT, 0);

        final Column[] activitiesColumns = {activityNameColumn, groupIdColumn, totalColumn};
        Table activitiesTable = new Table(activities, activitiesColumns);

        final String comment = "comment";
        final String postId = "p_id";
        final String activity_id = "a_id";

        final Column commentColumn = new Column(comment, SqlWrapper.TEXT, null);
        final Column postIdColumn = new Column(postId, SqlWrapper.INT, true);
        final Column sessionStartTimeColumn = new Column(true, startDate, SqlWrapper.DATE);
        final Column sessionEndTimeColumn = new Column(true, endDate, SqlWrapper.DATE);
        final Column activityIdColumn = Column.createForeignKey(activity_id, activitiesTable, SqlWrapper.ROWID);

        final String recordedSessions = "recordedsessions";

        final Column[] recordedSessionsColumns = {sessionStartTimeColumn, sessionEndTimeColumn, postIdColumn, commentColumn, activityIdColumn};
        Table recordedSessionsTable = new Table(recordedSessions, recordedSessionsColumns);

        try {
            final Connection con = SqlWrapper.connect();
            con.setAutoCommit(false);

            final Statement st = con.createStatement();
            st.execute(SqlWrapper.create(groupTable));
            st.executeUpdate(SqlWrapper.insert(groupTable, ALL));
            ResultSet rs = st.executeQuery(SqlWrapper.select(activityNameColumn, groupTable));
            SqlWrapper.printHistory();

            assertEquals(ALL, rs.getString(1));
            rs = st.executeQuery(SqlWrapper.select(activityNameColumn, groupTable, SqlWrapper.ROWID, 1));
            assertEquals(rs.getString(1), ALL);
            rs = st.executeQuery(SqlWrapper.select(activityNameColumn, groupTable, activityNameColumn, ALL));
            assertEquals(rs.getString(1), ALL);

            st.execute(SqlWrapper.create(activitiesTable));
            st.executeUpdate(SqlWrapper.insert(activitiesTable, maintenance));
            rs = st.executeQuery(SqlWrapper.selectAll(activitiesTable));
            assertEquals(rs.getString(1), maintenance);
            st.execute(SqlWrapper.create(recordedSessionsTable));


            st.execute(SqlWrapper.dropTable(recordedSessionsTable));
            st.execute(SqlWrapper.dropTable(activitiesTable));
            st.execute(SqlWrapper.dropTable(groupTable));

            con.commit();
            con.close();
        } catch (Exception e) {
            SqlWrapper.printHistory();
            e.printStackTrace();
        }
    }

    @Test
    public void memorizeEasyTest() {
        final String WORDS_TABLE = "words";
        final String DEF_TABLE = "definitions";
        final String MENG_TABLE = "meanings";
        final String CYC_TABLE = "cycle";
        final String SEQ_TABLE = "sequence";
        final String SCHED_TABLE = "scheduler";
        final String LAST_SCHED = "time";
        final String WORDS_ID = "w_id";
        final String WORD = "word";
        final String DEF_ID = "d_id";
        final String DEF = "def";
        final String CYC_NO = "c_id";
        final String PUBLISHED = "published";
        final String PUB_COUNT = "publishedCount";
        final String mengId = "meaningId";

        final Column wordColumn = new Column(WORD, SqlWrapper.TEXT);
        final Table wordsTable = new Table(WORDS_TABLE, wordColumn);

        final Column defColumn = new Column(DEF, SqlWrapper.TEXT);
        final Table defTable = new Table(DEF_TABLE, defColumn);

        final Column wordsIdColumn = Column.createForeignKey(WORDS_ID, wordsTable, SqlWrapper.ROWID);
        final Column defIdColumn = Column.createForeignKey(DEF_ID, defTable, SqlWrapper.ROWID);

        final Column[] mengColumns = {wordsIdColumn, defIdColumn};
        final Table mengTable = new Table(MENG_TABLE, mengColumns, mengColumns);

        final Column publishedColumn = new Column(PUBLISHED, SqlWrapper.BOOL, false, false);
        final Column publishCount = new Column(PUB_COUNT, SqlWrapper.INT, 0);
        final Column[] seqColumns = {publishedColumn, publishCount};
        final Table seqTable = new Table(SEQ_TABLE, seqColumns);

        final Column cycNoColumn = Column.createForeignKey(CYC_NO, seqTable, SqlWrapper.ROWID);
        final Column mengIdColumn = Column.createForeignKey(mengId, mengTable, SqlWrapper.ROWID);
        final Column[] cycleColumns = {cycNoColumn, mengIdColumn};
        final Table cycleTable = new Table(CYC_TABLE, cycleColumns, mengTable, mengIdColumn);

        final Column lastSchedColumn = new Column(LAST_SCHED, SqlWrapper.DATE);
        final Table schedTable = new Table(SCHED_TABLE, lastSchedColumn);

        try {
            final Connection con = SqlWrapper.connect();
            con.setAutoCommit(false);

            final Statement st = con.createStatement();
            st.execute(SqlWrapper.create(wordsTable));
            st.execute(SqlWrapper.create(defTable));
            st.execute(SqlWrapper.create(mengTable));
            st.execute(SqlWrapper.create(seqTable));
            st.execute(SqlWrapper.create(cycleTable));
            st.execute(SqlWrapper.create(schedTable));
            st.executeUpdate((SqlWrapper.insert(schedTable, lastSchedColumn, new Date(0))));
            st.executeUpdate(SqlWrapper.setNull(schedTable, lastSchedColumn, schedTable.getIdColumn(), SqlWrapper.index));

            st.execute(SqlWrapper.dropTable(schedTable));
            st.execute(SqlWrapper.dropTable(cycleTable));
            st.execute(SqlWrapper.dropTable(seqTable));
            st.execute(SqlWrapper.dropTable(mengTable));
            st.execute(SqlWrapper.dropTable(defTable));
            st.execute(SqlWrapper.dropTable(wordsTable));

            con.commit();
            con.close();
        } catch (Exception e) {
            SqlWrapper.printHistory();
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testStcal() throws Exception {
        final long FGPOST_ID = -1;

        final String groupId = "g_id";
        final String group = "groups";
        final String activityName = "name";
        final String maintenance = "maintenance";
        final Column activityNameColumn = new Column(activityName, SqlWrapper.TEXT);

        final Table groupTable = new Table(group, activityNameColumn);

        final String activities = "activities";
        final String total = "total";
        final String startDate = "start_date";
        final String endDate = "end_date";
        final String ALL = "all";

        final Column groupIdColumn = Column.createForeignKey(groupId, groupTable, SqlWrapper.ROWID);
        final int INF = Integer.MAX_VALUE;
        final Column activityStartDateColumn = new Column(startDate, SqlWrapper.DOUBLE, INF);
        final Column activityEndDateColumn = new Column(endDate, SqlWrapper.DOUBLE, FGPOST_ID);
        final Column totalColumn = new Column(total, 0, SqlWrapper.INT, 0);

        final Column[] activitiesColumns = {activityNameColumn, groupIdColumn, activityStartDateColumn, activityEndDateColumn, totalColumn};
        final Table activitiesTable = new Table(activities, activitiesColumns);

        final String comment = "comment";
        final String postId = "p_id";
        final String activity_id = "a_id";

        final Column commentColumn = new Column(comment, SqlWrapper.TEXT, "n/a");
        final Column postIdColumn = new Column(postId, SqlWrapper.INT, FGPOST_ID);
        final Column sessionStartTimeColumn = new Column(startDate, SqlWrapper.DOUBLE);
        final Column sessionEndTimeColumn = new Column(endDate, SqlWrapper.DOUBLE);
        final Column activityIdColumn = Column.createForeignKey(activity_id, activitiesTable, SqlWrapper.ROWID);

        final String recordedSessions = "recordedsessions";

        final Column[] recordedSessionsColumns = {sessionStartTimeColumn, sessionEndTimeColumn, postIdColumn, commentColumn, activityIdColumn};
        final Table recordedSessionsTable = new Table(recordedSessions, recordedSessionsColumns);


        final Connection con = SqlWrapper.connect();
        con.setAutoCommit(false);

        final Statement st = con.createStatement();
        st.execute(SqlWrapper.create(groupTable));
        st.executeUpdate(SqlWrapper.insert(groupTable, ALL));
        st.execute(SqlWrapper.create(activitiesTable));
        st.executeUpdate(SqlWrapper.insert(activitiesTable, maintenance));
        st.execute(SqlWrapper.create(recordedSessionsTable));

        final double startTime = 3d;
        final double endTime = 55.4d;
        final double activityId = st.executeQuery(SqlWrapper.select(SqlWrapper.ROWID, activitiesTable, activityNameColumn, maintenance)).getDouble(SqlWrapper.index);
        final double[] values = {startTime, endTime, activityId};
        final Column[] columns = {sessionStartTimeColumn, sessionEndTimeColumn, activityIdColumn};
        final int timeIncrement = (int) (endTime - startTime);

        st.executeUpdate(SqlWrapper.insert(recordedSessionsTable, columns, values));
        ResultSet rs = st.executeQuery(SqlWrapper.selectAll(recordedSessionsTable));
        assertTrue(rs.getDouble(sessionStartTimeColumn.getName()) == startTime);
        assertTrue(rs.getDouble(sessionEndTimeColumn.getName()) == endTime);
        assertTrue(rs.getDouble(activityIdColumn.getName()) == activityId);

        rs = st.executeQuery(SqlWrapper.select(totalColumn, activitiesTable));
        assertEquals(rs.getInt(totalColumn.getName()), 0);

        st.executeUpdate(SqlWrapper.increment(activitiesTable, totalColumn, timeIncrement, SqlWrapper.ROWID, activityId));
        rs = st.executeQuery(SqlWrapper.select(totalColumn, activitiesTable));
        assertEquals(rs.getInt(totalColumn.getName()), timeIncrement);

        double newEndTime = 3242323.53f;
        st.executeUpdate(SqlWrapper.update(recordedSessionsTable, sessionEndTimeColumn, newEndTime, sessionStartTimeColumn, startDate));

        rs = st.executeQuery(SqlWrapper.select(columns, recordedSessionsTable, sessionEndTimeColumn, newEndTime));
        assertTrue(rs.getDouble(sessionEndTimeColumn.getName()) == newEndTime);

        st.execute(SqlWrapper.dropTable(recordedSessionsTable));
        st.execute(SqlWrapper.dropTable(activitiesTable));
        st.execute(SqlWrapper.dropTable(groupTable));

        con.commit();
        con.close();
    }
}
